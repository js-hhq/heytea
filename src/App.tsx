import { BrowserRouter, Routes, Route } from "react-router-dom";

import NavBar from "@components/NavBar";
import router from "./router";
import styles from "./App.module.less";

export default function App() {
  return (
    <div className={styles.app}>
      <BrowserRouter>
        <section className={styles.main}>
          <Routes>
            {router.map((route) => (
              <Route key={route.path} path={route.path} element={route.component} />
            ))}
          </Routes>
        </section>
        <section className={styles.navBar}>
          <NavBar links={router} />
        </section>
      </BrowserRouter>
    </div>
  );
}
