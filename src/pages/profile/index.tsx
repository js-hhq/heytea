import Card from "@components/Card";
import ProfileHeader from "./ProfileHeader";
import styles from "./index.module.less";
import Swiper from "@components/Swiper";

export default function Profile() {
  return (
    <div className={styles.container}>
      <ProfileHeader />
      <div className={styles.wrapper}>
        <Card title="我的卡券">
          <div className={styles.coupon}>
            <div className={styles.couponItem}>
              <span>1</span>
              <span>喜茶券</span>
            </div>
            <div className={styles.couponItem}>
              <span>0</span>
              <span>喜卡</span>
            </div>
            <div className={styles.couponItem}>
              <span>1</span>
              <span>喜茶袋</span>
            </div>
            <div className={styles.couponItem}>
              <span>1</span>
              <span>积分商城</span>
            </div>
          </div>
        </Card>
        <Swiper />
        <Card title="贵宾服务" className={styles.common}>
          <div className={styles.service}>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>学子卡</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>阿喜熟客群</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>阿喜暖心券</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>我的客服</span>
            </div>
          </div>
          <div className={styles.service}>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>喜讯</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>兑换中心</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>发票助手</span>
            </div>
            <div className={styles.serviceItem}>
              <span>1</span>
              <span>更多</span>
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
}
