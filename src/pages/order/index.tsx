import { useState } from "react";
import styles from "./index.module.less";
import classnames from "classnames";
import OrderDetail from "@components/OrderDetail";

export default function Order() {
  const [currentTabIndex, setCurrentTabIndex] = useState(0);

  const handleTabClick = (index: number) => {
    setCurrentTabIndex(index);
  };

  return (
    <div className={styles.order}>
      <div className={styles.header}>
        <div className={styles.tabs}>
          <span
            onClick={() => handleTabClick(0)}
            className={classnames([
              styles.title1,
              { [styles.active]: currentTabIndex === 0 },
            ])}
          >
            茶饮鲜食
          </span>
          <span
            onClick={() => handleTabClick(1)}
            className={classnames([
              styles.title2,
              { [styles.active]: currentTabIndex === 1 },
            ])}
          >
            喜茶百货
          </span>
        </div>
      </div>
      <div className={styles.orderList}>
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
        <OrderDetail />
      </div>
    </div>
  );
}
