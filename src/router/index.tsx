import Home from "../pages/home";
import Goods from "../pages/goods";
import Order from "../pages/order";
import Shopping from "../pages/shopping";
import Profile from "../pages/profile";

export interface RouteOption {
  text: string;
  path: string;
  component: React.ReactElement;
  icon: React.ReactNode;
}

const router: RouteOption[] = [
  {
    text: "首页",
    path: "/",
    component: <Home />,
    icon: "",
  },
  {
    text: "点单",
    path: "/shopping",
    component: <Shopping />,
    icon: "",
  },
  {
    text: "百货",
    path: "/goods",
    component: <Goods />,
    icon: "",
  },
  {
    text: "订单",
    path: "/order",
    component: <Order />,
    icon: "",
  },
  {
    text: "我的",
    path: "/profile",
    component: <Profile />,
    icon: "",
  },
];

export default router;
