import { PropsWithChildren } from "react"
import styles from "./index.module.less"

interface CardProps {
  title: string
  className?: string 
}

export default function Card({children, title, className}: PropsWithChildren<CardProps>) {
  return (<div className={`${styles.card} ${className}`}>
    <span className={styles.title}>{title}</span>
    {children}
  </div>)
}