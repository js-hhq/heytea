import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import classnames from "classnames";

import styles from "./index.module.less";
import type { RouteOption } from "@router/index";

export default function NavBar({ links }: { links: RouteOption[] }) {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const [currentActive, setCurrentActive] = useState<number>(
    links.findIndex((link) => link.path === pathname)
  );

  const handleLinkClick = (index: number) => {
    if (index === currentActive) return;
    setCurrentActive(index);
    navigate(links[index].path);
  };

  return (
    <ul className={styles.container}>
      {links.map((link, index) => (
        <a
          onClick={() => handleLinkClick(index)}
          className={classnames([
            styles.link,
            { [styles.active]: currentActive === index },
          ])}
          key={link.text}
        >
          {link.icon ? <span>{link.icon}</span> : null}
          <li className={styles.text}>{link.text}</li>
        </a>
      ))}
    </ul>
  );
}
