import styles from "./index.module.less"

interface OrderProps {
  dest: string;
  time: string;
  status: "已完成" | "制作中" | "已取消";
  price: number;
  total: number;
  imgs: string[]
}

export default function OrderDetail() {
  return (<div className={styles.container}>
    <div className={styles.header}>
      <span className={styles.dest}>{"深圳福田京基KK ONE店"}</span>
      <span className={styles.status}>已完成</span>
    </div>
    <div className={styles.time}>{"2024-03-15 13:10:33"}</div>
    <div className={styles.imgWrapper}>
      <div className={styles.imgs}></div>
      <div className={styles.price}>
        <span className={styles.priceText}>¥126</span>
        <span>共7件</span>
      </div>
    </div>

    <div className={styles.buttons}>
      <button>开发票</button>
      <button>评价一下</button>
      <button className={styles.again}>再来一单</button>
    </div>
  </div>)
}