declare module "postcss-px-to-viewport" {
  type Options = {
    unitToConvert: 'px' | "rem" | "cm" | "em"
    viewportWidth: number
    unitPrecision: number
    propList: string[]
    viewportUnit: string
    fontViewportUnit: string
    selectorBlackList: string[]
    minPixelValue:number
    mediaQuery: boolean
    replace: boolean
    exclude: (string | RegExp)[]   
    landscape: boolean
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export default function(options: Partial<Options>): any;
}